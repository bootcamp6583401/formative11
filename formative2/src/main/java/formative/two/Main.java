package formative.two;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/")
public class Main extends HttpServlet {

    private static final long serialVersionUID = 2345345L;

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm");
        req.setAttribute("currentTime", currentTime.format(formatter));

        req.getRequestDispatcher("/result.jsp").forward(req, res);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<Person> persons = createPersonList();
        session.setAttribute("persons", persons);

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    private List<Person> createPersonList() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("John", "Doe", "john.doe@example.com"));
        persons.add(new Person("Jane", "Smith", "jane.smith@example.com"));
        persons.add(new Person("Bob", "Johnson", "bob.johnson@example.com"));
        persons.add(new Person("Alice", "Williams", "alice.williams@example.com"));
        persons.add(new Person("Charlie", "Brown", "charlie.brown@example.com"));
        return persons;
    }
}