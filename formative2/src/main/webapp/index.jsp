<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Person List</title>
</head>
<body>
    <h2>Person List</h2>
    <ul>
        <% for (formative.two.Person person : (java.util.List<formative.two.Person>)session.getAttribute("persons")) { %>
            <li><a href="details.jsp?firstName=<%= person.getFirstName() %>&lastName=<%= person.getLastName() %>&email=<%= person.getEmail() %>">
                    <%= person.getFirstName() %> <%= person.getLastName() %></a></li>
        <% } %>
    </ul>
</body>
</html>