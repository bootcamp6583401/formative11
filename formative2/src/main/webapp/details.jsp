<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Person Details</title>
</head>
<body>
    <h2>Person Details</h2>
    <p>First Name: ${param.firstName}</p>
    <p>Last Name: ${param.lastName}</p>
    <p>Email: ${param.email}</p>
    <p><a href="/">Back to Person List</a></p>
</body>
</html>