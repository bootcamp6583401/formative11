
package formative.one;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/app")
public class Main extends HttpServlet {

    private static final long serialVersionUID = 2345345L;

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm");
        req.setAttribute("currentTime", currentTime.format(formatter));

        req.getRequestDispatcher("/result.jsp").forward(req, res);

    }
}