<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta charset="UTF-8">
    <title>Result Page</title>
</head>
<body>
    <h2>Result Page</h2>
    <p>Current Time: <%= request.getAttribute("currentTime") %></p>
</body>
</html>